package com.ncube.backendtask.service;

import com.ncube.backendtask.dto.MemberDto;
import com.ncube.backendtask.dto.NewMemberDto;
import com.ncube.backendtask.exception.MemberNotFoundException;
import com.ncube.backendtask.model.Member;
import com.ncube.backendtask.repositories.MemberRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class MemberServiceImpl implements MemberService {
    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired
    private MemberRepository repository;

    @Override
    public Member createNewMember(final NewMemberDto newMemberDto, final MultipartFile picture) {
        byte [] pictureBytes = new byte[0];
        try {
            pictureBytes = picture.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        final Member member = new Member(newMemberDto, pictureBytes, picture.getContentType());
        repository.save(member);
        LOG.info("Member created.");
        return getMember(member.getId());
    }

    @Override
    public Member getMember(final String id) {
        Optional<Member> member = repository.findById(id);
        return member.orElseThrow(MemberNotFoundException::new);
    }

    @Override
    public Member updateMember(final String id, final MemberDto memberDto, final MultipartFile picture) {
        Member member = getMember(id);
        member.setFirstName(memberDto.getFirstName());
        member.setLastName(memberDto.getLastName());
        member.setZip(memberDto.getZip());
        member.setFirstName(memberDto.getFirstName());
        member.setBirthDate(memberDto.getBirthDate());
        try {
            member.setPicture(picture.getBytes());
            member.setPictureContentType(picture.getContentType());
        } catch (IOException e) {
            LOG.error("Can't update member's photo!");
        }
        repository.save(member);
        LOG.info("Member update with params:first_name: {}, last_name: {}, zip: {}, birth_date: {}, picture {}",
                memberDto.getFirstName(), memberDto.getLastName(), memberDto.getZip(), memberDto.getBirthDate(),
                picture.getName());

        return member;
    }

    @Override
    public boolean deleteMember(final String id) {
        final Member member = getMember(id);
        if (member != null) {
            repository.delete(member);
            return true;
        }
        LOG.info("No member with ID:{}", id);
        return false;
    }

    @Override
    public List<Member> getAllMembers() {
        return repository.findAll();
    }
}
