package com.ncube.backendtask.service;

import com.ncube.backendtask.dto.MemberDto;
import com.ncube.backendtask.dto.NewMemberDto;
import com.ncube.backendtask.model.Member;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MemberService {

    Member createNewMember(NewMemberDto newMemberDto, MultipartFile picture);

    Member getMember(String id);

    Member updateMember(String id, MemberDto memberDto, MultipartFile picture);

    boolean deleteMember(String id);

    List<Member> getAllMembers();

}
