package com.ncube.backendtask.controller;

import com.ncube.backendtask.dto.MemberDto;
import com.ncube.backendtask.dto.NewMemberDto;
import com.ncube.backendtask.exception.MemberNotFoundException;
import com.ncube.backendtask.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ncube.backendtask.model.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/members")
public class MembersController {
    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired
    private MemberService memberService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Member> getAllMembers() {
        LOG.info("Getting all members.");
        return memberService.getAllMembers();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Member getMemberById(@PathVariable("id") String id) {
        LOG.info("Getting member with ID: {}.", id);
        final Member member = memberService.getMember(id);
        if (member != null) {
            return member;
        } else {
            throw new MemberNotFoundException();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Member updateMemberById(@PathVariable("id") String id, @RequestPart("picture") MultipartFile picture, @Valid @RequestPart("member") MemberDto memberDto) {
        LOG.info("Update Member with ID: {}.", id);
        Member member = memberService.updateMember(id, memberDto, picture);
        if (member != null) {
            return member;

        } else {
            throw new MemberNotFoundException();
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public Member createMember(@RequestPart("picture") MultipartFile picture, @Valid @RequestPart("member") NewMemberDto newMemberDto) {
        return memberService.createNewMember(newMemberDto, picture);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteMember(@PathVariable String id) {
        if (memberService.deleteMember(id)) {
            LOG.info("Delete Member with ID: {}.", id);
            return ResponseEntity.ok("deleted");
        } else {
            throw new MemberNotFoundException();
        }
    }
}