package com.ncube.backendtask.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No such Member")
public class MemberNotFoundException extends RuntimeException {
}
