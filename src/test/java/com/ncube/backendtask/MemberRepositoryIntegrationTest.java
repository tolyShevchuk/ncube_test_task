package com.ncube.backendtask;

import com.ncube.backendtask.controller.MembersController;
import com.ncube.backendtask.dto.NewMemberDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BackendTaskApplication.class, MembersController.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class MemberRepositoryIntegrationTest {
    private RestTemplate restTemplate = new RestTemplate();
    private final static String url = "http://localhost:8102/members";

    @Test
    public void MemberApi_CreateMember_ShouldCreateMember() {
        FileSystemResource fileSystemResource = new FileSystemResource("test_ava.jpg");
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("member", new NewMemberDto("first", "last", new Date(), "11"));
        map.add("picture", fileSystemResource);
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth("username", "password");
        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(map, headers);
        ResponseEntity<Map> responseEntity = restTemplate.postForEntity(url, entity, Map.class);
        assertEquals(200, responseEntity.getStatusCodeValue());
        restTemplate.delete(url + String.valueOf(Objects.requireNonNull(responseEntity.getBody()).get("id")));
    }

    @Test
    public void MemberApi_UpdateMember_ShouldUpdateMember() {
        FileSystemResource fileSystemResource = new FileSystemResource("test_ava.jpg");
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("member", new NewMemberDto("first", "last", new Date(), "11"));
        map.add("picture", fileSystemResource);
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth("username", "password");
        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(map, headers);
        ResponseEntity<Map> responseEntity = restTemplate.postForEntity(url, entity, Map.class);
        assertEquals(200, responseEntity.getStatusCodeValue());
        String testUserKey = String.valueOf(String.valueOf(Objects.requireNonNull(responseEntity.getBody()).get("id")));
        Map<String, String> params = new HashMap<>();
        params.put("id", testUserKey);
        restTemplate.put(url + testUserKey, entity, Map.class);
    }

    @Test
    public void MemberApi_GetMember_ShouldGetMember() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth("username", "password");

        FileSystemResource fileSystemResource = new FileSystemResource("test_ava.jpg");
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("member", new NewMemberDto("first", "last", new Date(), "11"));
        map.add("picture", fileSystemResource);
        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(map, headers);
        ResponseEntity<Map> responseEntity = restTemplate.postForEntity(url, entity, Map.class);
        assertEquals(200, responseEntity.getStatusCodeValue());

        FileSystemResource fileSystemResource2 = new FileSystemResource("test_ava.jpg");
        MultiValueMap<String, Object> map2 = new LinkedMultiValueMap<>();
        map2.add("member", new NewMemberDto("first2", "last2", new Date(), "12"));
        map2.add("picture", fileSystemResource2);
        HttpEntity<MultiValueMap<String, Object>> entity2 = new HttpEntity<>(map2, headers);
        ResponseEntity<Map> responseEntity2 = restTemplate.postForEntity(url, entity2, Map.class);
        assertEquals(200, responseEntity2.getStatusCodeValue());



        ResponseEntity<List> responseGetEntity = restTemplate.getForEntity(url, List.class);
        assertEquals(200, responseGetEntity.getStatusCodeValue());
    }


}