FROM openjdk:8-jdk-alpine
LABEL maintainer="toly.shevchuk@gmail.com"
VOLUME /tmp
EXPOSE 8102
ARG JAR_FILE=target/backendtask-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} backendtask.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/backendtask.jar"]