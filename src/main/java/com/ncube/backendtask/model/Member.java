package com.ncube.backendtask.model;

import com.ncube.backendtask.dto.NewMemberDto;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;

@Document(collection = "member")
public class Member {
    @Id
    private String id;

    @Field("first_name")
    @NotEmpty(message = "first_name can not be empty.")
    @Size(max = 100, message = "first_name can not be more than 100 characters.")
    private String firstName;

    @Field("last_name")
    @NotEmpty(message = "last_name can not be empty.")
    @Size(max = 100, message = "last_name can not be more than 100 characters.")
    private String lastName;

    @Field("birth_date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date birthDate;

    @Field("zip")
    private String zip;

    @Field("picture")
    private byte[] picture;

    @Field("picture_content_type")
    private String pictureContentType;

    public Member() {
    }

    public Member(final NewMemberDto newMemberDto, final byte[] picture, final String pictureContentType) {
//        this.setId(ObjectId.get());
        this.setFirstName(newMemberDto.getFirstName());
        this.setLastName(newMemberDto.getLastName());
        this.setZip(newMemberDto.getZip());
        this.setFirstName(newMemberDto.getFirstName());
        this.setBirthDate(newMemberDto.getBirthDate());
        if (picture != null) {
            this.setPicture(picture);
            this.setPictureContentType(pictureContentType);
        }
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getZip() {
        return zip;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] image) {
        this.picture = image;
    }

    public String getPictureContentType() {
        return pictureContentType;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }
}
